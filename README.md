# ansible-desktop

This is an Ansible configuration for provisioning a laptop development machine using an Ubuntu Desktop.

## Software provisioned

* OpenJDK: role andrewrothstein.openjdk
* Git: role geerlingguy.git
* VirtualBox role: crivetimihai.virtualization.virtualbox
* Docker role: crivetimihai.virtualization.docker
* Vagrant role: crivetimihai.virtualization.vagrant
* Eclipse STS role: by-erik.sts
* Slack role: oefenweb.slack
* Microsoft Teams role: alvistack.teams
* Firefox (deb version) role: alvistack.firefox
* OBS role: alvistack.obs_studio
* Ansible: role: alvistack.ansible
* VLC: role: alvistack.vlc
* VS Code role: alvistack.code
* RClone role: alvistack.rclone
* Python role: alvistack.python
* Helm role: alvistack.helm

## Pending

* Codium (instead of vs code), including extensions

## Usage

### Bootstraping

```bash
sudo apt update && sudo apt install ansible
git clone https://gitlab.com/gortazar/ansible-desktop.git
cd ansible desktop
ansible-galaxy install -r requirements.txt
```

### Install

ansible-playbook local-roles.yml
